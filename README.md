Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

DIRECTORY STRUCTURE
-------------------
~~~
 Our Api Directory
      api/v1              contains our user rest api
      api/config          contains rest api configurations
      api/modules         contains rest api vesion folders
      api/modules/v*      contains rest api different vesion controller and models
      api/config          contains rest api configurations
  Default from framework
      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources
~~~


REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


CLONE THROUGH GITHUB
------------

~~~
git clone https://devadh@bitbucket.org/madan21/user.git
~~~

DATABASE
------------
Database Name - yii_user_rest
You can import database from file. The sql file are in root directory.
~~~

user.sql
~~~

API means root folder in whole README
------------
GET LIST OF USER
------------
HTTP Method - GET
Url - API/api/v1/user

RESPONSE
~~~
SUCCESS
  {
    "status": "success",
    "data": [
      {
        "id": 15,
         ....
      },
      .....
    ]
  }
~~~

CREATE USER
------------
HTTP Method - POST
Url - API/api/v1/user

RESPONSE
~~~
SUCCESS
  {
    "status": "success",
    "message": "Success message"
  }

ERROR
  {
    "status": "error",
    "errors": {
      "email": [
        "validation error"
      ],
      .......
    }
  }
~~~

VIEW USER BY ID

HTTP Method - GET
Url - API/api/v1/user/1

RESPONSE
~~~
SUCCESS
  {
    "status": "success",
    "data": {
      "id": 15,
      ...
    }
  }

ERROR
  {
    "status": "error",
    "message": "Error message"
  }
~~~

UPDATE USER BY ID
------------
HTTP Method - PUT, PATCH
Url - API/api/v1/user/1

RESPONSE
~~~
SUCCESS
  {
    "status": "success",
    "message": "Success message"
  }

ERROR
  {
    "status": "error",
    "message": "Error message"
  }
~~~

DELETE USER BY ID
------------
HTTP Method - DELETE
Url - API/api/v1/user/1

RESPONSE
~~~
SUCCESS
  {
    "status": "success",
    "message": "Success message"
  }

ERROR
  {
    "status": "error",
    "message": "Error message"
  }
~~~
