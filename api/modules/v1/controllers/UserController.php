<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use api\modules\v1\models\User;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\Cors;
/**
 * Class UserController
 * @package api\modules\v1\controllers
 */

class UserController extends ActiveController
{
    /**
    * @modelClass
    */
    public $modelClass = 'api\modules\v1\models\User';

      /**
    * @var array Response send to the client (format json)
    */
    public $response = [];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            //@TODO if for any action need to change response format
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => [], //add action here
                'formats' => [
                    'application/json' => Response::FORMAT_JSON // response only for index @TODO
                ],
            ],
            //@TODO add allowed domains url to Origin []
            'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'GET'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Headers' => ['Content-Type'],
                ],
            ],
            //@TODO allowed HTTP request methods for each action
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index'  => ['GET'],
                    'view'   => ['GET'],
                    'create' => ['POST'],
                    'update' => ['GET', 'PUT', 'POST','PATCH'],
                    'delete' => ['POST', 'DELETE'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset( $actions['index'], $actions['create'], $actions['view'], $actions['update'], $actions['delete']);
        return $actions;
    }

    /**
    * Get all list of user
    * @method GET
    * @return Array
    */
    public function actionIndex()
    {
        $this->response['status'] = 'success';
        \Yii::$app->response->setStatusCode(200);
        $model = User::find()->all();
        if($model){
            $this->response['data'] = $model;
        }else{
            $this->response['data'] = 'Empty Database';
        }
        return $this->response;
    }

    /**
    * Insert user data
    * @method POST
    * @return Array
    */
    public function actionCreate()
    {
        $this->response['status'] = 'error';
        $model = new User();
        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post(),'') && $model->validate()){
            $model->save();
            $this->response['status'] = 'success';
            $this->response['message'] = 'Data inserted successfully';
        }else{
            $this->response['errors'] = $model->getErrors();
        }
        return $this->response;
    }

    /**
    * Get user by id
    * @method GET
    * @return Array
    */
    public function actionView($id)
    {
        $this->response['status'] = 'error';
        $model = User::findOne(['id' => $id ]);
        if(!$model)
        {
            $this->response['message'] = $id .'  id not found';
        }
        else
        {
            $this->response['status'] = 'success';
            $this->response['data'] = $model;
        }
        return $this->response;
    }

    /**
    *@params $id
    *  @method GET, POST
     * @return array
     */
    public function actionUpdate($id)
    {
        $this->response['status'] = 'error';
        $model = User::findOne(['id' => $id ]);
        if($model && $model->load(\Yii::$app->request->post(),''))
        {
            $model->save();
            $this->response['status'] = 'success';
            $this->response['message'] = 'Data for id '.$id.' successfully updated';
        }
        else
        {
            $this->response['message'] = $id .'  id not found';
        }
        return $this->response;
    }

    public function actionDelete($id)
    {
        $this->response['status'] = 'error';
        $model = User::findOne(['id' => $id ]);
        if($model)
        {
            $delete = User::findOne(['id' => $id ])->delete();
            if($delete){
                $this->response['status'] = 'success';
                $this->response['message'] = 'Data for id '.$id.' successfully deleted';
            }else{
                $this->response['message'] = 'Cannot delete data';
            }
        }
        else
        {
            $this->response['message'] = $id .'  id not found';
        }
        return $this->response;
    }

}
