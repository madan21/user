<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $birthday
 * @property string $gender
 * @property string $created_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'birth_day', 'gender'], 'required'],
            [['name'], 'string', 'min' => 4, 'max' => 25],
            [['email'], 'string', 'max' => 50],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['birth_day'], 'date', 'format' => 'php:Y-m-d', 'message' => Yii::t('app', 'Date must be in format "Y-m-d"')],
            [['gender'], 'in', 'range' => ['Male','Female'], 'message' => Yii::t('app', 'Gender must be "Male" or "Female"')],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'birth_day' => 'Birth Day',
            'gender' => 'Gender',
            'created_at' => 'Created At',
        ];
    }
}
